# Encoding: UTF-8
#
# BRCP - C7N Runner - Security Groups
#
# 2019 Brian Dwyer - Broadridge Financial Solutions
#

resource "aws_security_group" "custodian_runner_autoscaler" {
  name_prefix = "BRCP-C7N-Runner-Autoscaler"
  description = "Broadridge Cloud Platform - C7N Runner Autoscaler"
  vpc_id      = data.aws_vpc.custodian_runner.id

  lifecycle {
    # TAINT THE GROUP TO MAKE THIS WORK PROPERLY
    create_before_destroy = true
  }

  ingress {
    description = "GitLab - Debug Terminal"
    from_port   = 8098
    to_port     = 8098
    protocol    = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }

  egress {
    description = "Allow All to Broadridge"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    description = "Allow AWS Outbound"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"

    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.aws_common_tags,
    {
      "Name" = "BRCP-C7N-Runner-Autoscaler"
    },
  )
}

resource "aws_security_group" "custodian_runner" {
  name_prefix = "BRCP-C7N-Runner"
  description = "Broadridge Cloud Platform - C7N Runner"
  vpc_id      = data.aws_vpc.custodian_runner.id

  lifecycle {
    # TAINT THE GROUP TO MAKE THIS WORK PROPERLY
    create_before_destroy = true
  }

  ingress {
    description = "SSH Ingress from Runner Autoscaler"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"

    security_groups = [
      aws_security_group.custodian_runner_autoscaler.id
    ]
    # cidr_blocks = [
    #   "10.0.0.0/8",
    # ]
  }

  ingress {
    description = "Docker over SSL"
    from_port   = 2376
    to_port     = 2376
    protocol    = "tcp"

    security_groups = [
      aws_security_group.custodian_runner_autoscaler.id
    ]
  }

  egress {
    description = "Allow AWS Outbound"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"

    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.aws_common_tags,
    {
      "Name" = "BRCP-C7N-Runner"
    },
  )
}
