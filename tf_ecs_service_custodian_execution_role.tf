# Encoding: UTF-8
#
# BRCP - C7N Runner Autoscaler - Execution Role
#
# 2019 Brian Dwyer - Broadridge Financial Solutions
#

resource "aws_iam_policy" "c7n_runner_autoscaler_task_execution" {
  name        = "c7n_runner_autoscaler_${local.aws_region}"
  path        = "/"
  description = "BRCP - C7N Runner Autoscaler - ECS Task Execution Policy (${local.aws_region})"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_role" "c7n_runner_autoscaler_task_execution" {
  name        = "c7n_runner_autoscaler_${local.aws_region}"
  path        = "/"
  description = "BRCP - C7N Runner Autoscaler - ECS Task Execution Role (${local.aws_region})"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "ecs-tasks.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "c7n_runner_autoscaler_task_execution1" {
  role       = aws_iam_role.c7n_runner_autoscaler_task_execution.name
  policy_arn = aws_iam_policy.c7n_runner_autoscaler_task_execution.arn
}

resource "aws_iam_role_policy_attachment" "c7n_runner_autoscaler_task_execution_ecr_policy" {
  role       = aws_iam_role.c7n_runner_autoscaler_task_execution.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
