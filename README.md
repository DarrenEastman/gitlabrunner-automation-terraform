## Custodian - Autoscaling Runners

- Author: Bryan Dwyer

## Overview

This deploys a master ECS task on AWS Fargate. This task can:

1. Register with GitLab.
2. Lifecycle EC2 instances on-demand for use by CI jobs.

The ECS task role must be able to:

1. SSH to the spawned instances.
2. Have Docker over SSL opened (2376) to the Surrogate EC2
3. Pull any ECR images defined in the Gitlab Pipeline (credential-helper/pull process runs on the ECS side, not the surrogate!)

## IAM Implications

### Task Role

- Lifecycle Instances
- Pass the Instance Role

### Instance Role

- Whatever heavy lifting you want to do (AssumeRole, etc.)

Code Source: GitLab customer
