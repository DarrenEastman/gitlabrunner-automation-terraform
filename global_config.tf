# Encoding: UTF-8

terraform {
  required_version = ">= 0.12"
}

locals {
  # Build out AWS Common Tags
  aws_common_tags = {
    ChargeCode = "DVADM-5225-550-00130-000-000000000000000"
    Contact    = "Brian.Dwyer@broadridge.com"
    Purpose    = "Custodian Runner"
    SBU        = "CSG"
  }

  # Runner Configuration Bucket - Per-Region
  runner_config_bucket = "${var.runner_config_bucket}-${local.env[local.aws_profile]}-${data.aws_region.current.name}"

  # Environment
  env = {
    tss_dev         = "dev"
    shared_services = "prd"
  }

  search = {
    tss_dev         = "tssdev"
    shared_services = "lzss-prd"
  }
}

variable "runner_cluster_name" {
  description = "The name of the ECS Cluster for Project Build Runners"
  default     = "CSG-Runner-Cluster"
}

variable "runner_config_bucket" {
  description = "The name of the S3 bucket to persist Runner Configuration & Cache"
  default     = "csg-runner-config"
}

variable "runner_name" {
  description = "The Name of the build runner & associated resources"
  default     = "CSG-Custodian-Runner"
}

variable "runner_role_name" {
  default = "csg_custodian_runner_autoscaler"
}
