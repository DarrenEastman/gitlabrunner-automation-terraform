#
# Cluster Runner - VPC Endpoint Access SG
#

#
# NOTE: This only seems to be present in LZ-SS
#

data "aws_security_group" "vpc_std_access" {
  count = local.aws_profile == "shared_services" ? 1 : 0
  filter {
    name   = "group-name"
    values = ["*StandardVPCAccess"]
  }
}
