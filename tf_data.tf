# Encoding: UTF-8

terraform {
  required_version = ">= 0.12"
}

locals {
  # Derive AWS Account & Region from Workspace
  aws_profile = element(split(".", terraform.workspace), 0)
  aws_region  = element(split(".", terraform.workspace), 1)
}

provider "aws" {
  region  = local.aws_region
  profile = local.aws_profile
}

data "aws_caller_identity" "current" {
}

data "aws_region" "current" {
}

data "aws_iam_role" "custodian_runner" {
  name = var.runner_role_name
}

data "aws_iam_instance_profile" "custodian_runner" {
  name = "c7n_ec2_runner"
}

data "aws_vpc" "custodian_runner" {
  filter {
    name   = "tag:Name"
    values = ["*${local.search[local.aws_profile]}*"]
  }
}

# Search for Matching Subnets
data "aws_subnet_ids" "custodian_runner" {
  vpc_id = data.aws_vpc.custodian_runner.id

  tags = {
    Name = "*${local.search[local.aws_profile]}*${local.aws_profile == "tss_dev" ? "app" : "prv*B"}*"
  }
}

# Search for Latest BR CoreOS AMI
data "aws_ami" "br_coreos" {
  most_recent = true
  owners = [
    "123456789876" # AMI Factory Account
  ]

  filter {
    name   = "name"
    values = ["csg-coreos*"]
  }

  filter {
    name   = "tag-key"
    values = ["CSG_Distribution"]
  }
}
