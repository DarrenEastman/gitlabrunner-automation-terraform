# Encoding: UTF-8
#
# BRCP - C7N Runner - Instance Role
#
# 2019 Brian Dwyer - Broadridge Financial Solutions
#


resource "aws_iam_instance_profile" "c7n_runner" {
  name = "c7n_ec2_runner"
  role = aws_iam_role.c7n_runner.name
}

resource "aws_iam_role" "c7n_runner" {
  name        = "c7n_ec2_runner"
  path        = "/"
  description = "BRCP - C7N Runner - Instance Role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "c7n_runner" {
  role       = aws_iam_role.c7n_runner.name
  policy_arn = aws_iam_policy.c7n_runner.arn
}

resource "aws_iam_policy" "c7n_runner" {
  name        = "c7n_ec2_runner"
  path        = "/"
  description = "BRCP - C7N EC2 Runner Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "sts:AssumeRole",
          "ec2:DescribeRegions"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "c7n_runner_CloudWatchAgent" {
  role       = aws_iam_role.c7n_runner.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

resource "aws_iam_role_policy_attachment" "c7n_runner_SSM" {
  role       = aws_iam_role.c7n_runner.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "c7n_runner_ECR" {
  role       = aws_iam_role.c7n_runner.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
