#
# Custodian Runner Autoscaler - IAM Policy
#

terraform {
  required_version = ">= 0.12"
}

locals {
  # Derive AWS Account & Region from Workspace
  aws_profile = terraform.workspace
  aws_region  = "us-east-1"
  # aws_region  = element(split(".", terraform.workspace), 1)
}

provider "aws" {
  profile = local.aws_profile
  region  = local.aws_region
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

resource "aws_iam_role" "custodian_runner_autoscaler" {
  name        = var.runner_role_name
  path        = "/"
  description = "ECS - Custodian Runner Autoscaler"

  assume_role_policy    = data.aws_iam_policy_document.ecs_task_assume_role.json
  force_detach_policies = true

  tags = local.aws_common_tags
}

#
# Allow an ECS Task to AssumeRole
#
data "aws_iam_policy_document" "ecs_task_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"

      identifiers = [
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role_policy_attachment" "custodian_runner_autoscaler_ecr_policy" {
  role       = var.runner_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "custodian_runner_autoscaler" {
  role       = var.runner_role_name
  policy_arn = aws_iam_policy.custodian_runner_autoscaler.arn
}

resource "aws_iam_policy" "custodian_runner_autoscaler" {
  name   = aws_iam_role.custodian_runner_autoscaler.name
  path   = "/"
  policy = data.aws_iam_policy_document.custodian_runner_autoscaler.json
}

#
# Define the Runner Autoscaler Policy
#
data "aws_iam_policy_document" "custodian_runner_autoscaler" {
  # Allow CRUD of Runner Instances
  statement {
    effect = "Allow"

    actions = [
      "ec2:*",
    ]

    resources = [
      "*",
    ]
  }

  # Allow Passing of Instance Roles
  statement {
    effect = "Allow"

    actions = [
      "iam:PassRole",
    ]

    resources = [
      "arn:aws:iam::*:role/*",
    ]
  }

  # Allow Access to Runner Configuration TOML
  statement {
    effect = "Allow"

    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::${local.runner_config_bucket}",
      "arn:aws:s3:::${local.runner_config_bucket}/${var.runner_name}-*/config.toml"
    ]
  }
}
