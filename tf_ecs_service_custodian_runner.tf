# Encoding: UTF-8
#
# BRCP - Custodian Runner
#
# 2019 Brian Dwyer - Broadridge Financial Solutions
#

# Locate the ECS Cluster
data "aws_ecs_cluster" "runners" {
  cluster_name = var.runner_cluster_name
}

# Locate the Runner Config Bucket
data "aws_s3_bucket" "runner_config" {
  bucket = local.runner_config_bucket
}

# Pick a Random Subnet
resource "random_shuffle" "custodian_runner_subnets" {
  input = data.aws_subnet_ids.custodian_runner.ids
}

data "aws_subnet" "custodian_runner" {
  id = random_shuffle.custodian_runner_subnets.result[0]
}

#
# Container Definitions Template
#
data "template_file" "custodian_runner_cdefs" {
  template = file("${path.module}/task-definitions/gitlab-runner.json")

  vars = {
    runner_name        = "${var.runner_name}-${local.aws_profile}"
    cluster            = "${data.aws_ecs_cluster.runners.cluster_name}--${data.aws_region.current.name}"
    region             = data.aws_region.current.name
    cw_log_group       = aws_cloudwatch_log_group.custodian_runner.name
    vpc                = data.aws_vpc.custodian_runner.id
    subnet             = data.aws_subnet.custodian_runner.id
    aws_zone           = substr(data.aws_subnet.custodian_runner.availability_zone, -1, 1)
    security_group     = aws_security_group.custodian_runner.name
    aws_ami            = data.aws_ami.br_coreos.id
    aws_ssh_user       = "core"
    instance_profile   = data.aws_iam_instance_profile.custodian_runner.name
    instance_type      = "t3a.2xlarge"
    gitlab_url         = "https://git.devops.broadridge.net"
    registration_token = "xh8LMyAbyKcysHzH1gki"
    docker_image       = "python:3.7-alpine"
    allowed_images     = "**/*"
    tag_list = join(",", [
      "${var.runner_name}-${upper(local.env[local.aws_profile])}",
      "${var.runner_name}-${upper(local.env[local.aws_profile])}-${data.aws_region.current.name}",
    ])
    config_bucket     = data.aws_s3_bucket.runner_config.id
    concurrent        = 4
    memoryReservation = 512
    session_port      = 8098
  }
}

resource "aws_ecs_task_definition" "custodian_runner" {
  family                = var.runner_name
  container_definitions = data.template_file.custodian_runner_cdefs.rendered

  network_mode = "awsvpc"

  task_role_arn = data.aws_iam_role.custodian_runner.arn

  # Secret Retrieval, etc.
  execution_role_arn = aws_iam_role.c7n_runner_autoscaler_task_execution.arn

  requires_compatibilities = ["FARGATE"]
  cpu                      = 512
  memory                   = 1024

  tags = local.aws_common_tags

  # Needs to be Persistent, otherwise lots of runner registrations
  # RexRay Task-Specific EBS (Per-Runner, Per-Region)
  # EFS (Per-Region, maybe mount /mnt/efs/gitlab-runner)

  volume {
    name = "runner-config"

    docker_volume_configuration {
      scope         = "shared"
      autoprovision = true
    }
  }

  volume {
    name = "runner-data"

    docker_volume_configuration {
      scope         = "shared"
      autoprovision = true
    }
  }
}

resource "aws_ecs_service" "custodian_runner" {
  name                    = "${var.runner_name}-${local.aws_profile}"
  cluster                 = data.aws_ecs_cluster.runners.id
  task_definition         = aws_ecs_task_definition.custodian_runner.arn
  enable_ecs_managed_tags = true

  launch_type = "FARGATE"

  tags = local.aws_common_tags

  # This configuration facilitates 1 runner per-region
  ## During service deployment, it will bring down the existing service.
  ## This allows the new runner to unregister the old one.  You cannot unregister an active runner.

  desired_count                      = 1
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0

  network_configuration {
    security_groups = [
      aws_security_group.custodian_runner_autoscaler.id
    ]

    subnets = data.aws_subnet_ids.custodian_runner.ids
  }

  // Placement Constraints only valid for EC2 Launch-Type
  // placement_constraints {
  //   # Ensure we only run One-per-Instance
  //   type = "distinctInstance"
  // }
}

#
# CloudWatch Logs Group
#
resource "aws_cloudwatch_log_group" "custodian_runner" {
  name              = "/ecs/custodian_runner_autoscaler"
  retention_in_days = 30

  tags = {
    Name    = "C7N-Runner-Autoscaler"
    Purpose = "C7N Runner Autoscaling Master"
    Contact = "brian.dwyer@broadridge.com"
  }
}
